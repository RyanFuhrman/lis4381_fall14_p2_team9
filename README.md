# Project 2 #


### Description ###

As a designer/developer, it’s important to be able to address your client’s
needs in a number of ways: design, development, advertising, marketing, data storage and
analytics, search engine optimization, etc. In this assignment, you will create a Web
application based upon a client’s existing documentation. 

### Considerations ###

Overall Effectiveness:

 Site has a particular purpose (objective). Did you succeed in meeting that purpose?

 General presentation/structure/coherence:

          o Writing style, grammar, spelling, etc.

          o Site structure, organization, usability, ease of navigation, (no dead-ends!)

          o Accessibility